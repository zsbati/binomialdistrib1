import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

class Main {
  public static void main(String[] args) {double pB = 1.09/(1.0+1.09);
    double pG = 1.0/(1.0+1.09);
    //probab. of at least 3 boys:
    double probab = Math.pow(pB,6)+6.0*Math.pow(pB,5)*pG+15.0*Math.pow(pB,4)*Math.pow(pG,2)+20.0*Math.pow(pB,3)*Math.pow(pG,3);
    probab =Math.round(1000.0*probab)/1000.0;
    System.out.println(probab);
    
  }
}
